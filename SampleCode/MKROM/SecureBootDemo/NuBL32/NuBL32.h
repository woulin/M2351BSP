/**************************************************************************//**
 * @file     NuBL32.h
 * @version  V3.00
 * @brief    NuBL32 header file.
 *
 * @copyright (C) 2019 Nuvoton Technology Corp. All rights reserved.
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "NuMicro.h"

extern const uint32_t g_InitialFWInfo[]; // A global variable to store NuBL32 FWINFO address, declared in FwInfo.c

/*** (C) COPYRIGHT 2019 Nuvoton Technology Corp. ***/
