/**************************************************************************//**
 * @file     ProcessVendorCmd.h
 * @version  V3.00
 * @brief    Process vendor command header file.
 *
 * @copyright (C) 2018 Nuvoton Technology Corp. All rights reserved.
 ******************************************************************************/
#ifndef PROCESSVENDORCMD_H
#define PROCESSVENDORCMD_H

extern ISP_INFO_T   g_ISPInfo;

#endif
/*** (C) COPYRIGHT 2018 Nuvoton Technology Corp. ***/
